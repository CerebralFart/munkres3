import React from 'react';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Authentication from "./Authentication";

class Munkres3 extends React.Component {
	render() {
		return (
			<BrowserRouter>
				<Switch>
					<Route key="auth" path="/auth" component={Authentication}/>
					<Route key="sel" path="/select" component={Authentication}/>
				</Switch>
			</BrowserRouter>
		);
	}
}

export default Munkres3;
