package canvas;

import lombok.extern.slf4j.Slf4j;
import munkres3.config.Config;

@Slf4j
public class API {
    private String location;

    public static API createInstance() {
        return new API(Config.get("canvas.location", String.class));
    }

    private API(String location) {
        this.location = location;
    }
}
