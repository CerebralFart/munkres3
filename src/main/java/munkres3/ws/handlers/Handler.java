package munkres3.ws.handlers;

import munkres3.ws.CommandException;
import munkres3.ws.Request;
import munkres3.ws.Response;

public interface Handler {
    String getKeyword();

    Response handle(Request request) throws CommandException;
}
