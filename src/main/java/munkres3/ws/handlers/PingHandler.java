package munkres3.ws.handlers;

import munkres3.ws.Request;
import munkres3.ws.Response;

public class PingHandler implements Handler {
    public String getKeyword() {
        return "ping";
    }

    public Response handle(Request request) {
        Response response = request.createResponse();
        response.setResponse("pong");
        return response;
    }
}
