package munkres3.ws.handlers;

import munkres3.ws.Request;
import munkres3.ws.Response;

public class ErrorHandler {
    public Response handle(Request request, Exception exception) {
        return new ErrorResponse(request.getCommand(), exception);
    }

    private class ErrorResponse extends Response {
        public ErrorResponse(String command, Exception exception) {
            super(command);
            this.status = "error";
        }
    }
}
