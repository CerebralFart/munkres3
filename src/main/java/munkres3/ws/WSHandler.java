package munkres3.ws;

import lombok.extern.slf4j.Slf4j;
import munkres3.ws.handlers.ErrorHandler;
import munkres3.ws.handlers.Handler;
import munkres3.ws.handlers.PingHandler;
import org.eclipse.jetty.websocket.api.RemoteEndpoint;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.WebSocketAdapter;
import org.eclipse.jetty.websocket.servlet.WebSocketServlet;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class WSHandler extends WebSocketAdapter {
    private static Map<String, Handler> handlers;
    private static final ErrorHandler errorHandler = new ErrorHandler();

    static {
        handlers = new HashMap<>(1);
        addHandler(new PingHandler());
    }

    private static void addHandler(Handler handler) {
        String keyword = handler.getKeyword();
        handlers.put(keyword, handler);
    }

    private Session session;

    @Override
    public void onWebSocketConnect(Session sess) {
        super.onWebSocketConnect(sess);
        this.session = sess;
        log.debug("Socket {} connected", sess.getRemoteAddress());
    }

    @Override
    public void onWebSocketText(String message) {
        log.debug("Recieved '{}' from {}", message, this.session.getRemoteAddress());
        super.onWebSocketText(message);
        Request request = Request.fromString(message);

        Response response;
        if (handlers.containsKey(request.getCommand())) {
            Handler handler = handlers.get(request.getCommand());
            try {
                response = handler.handle(request);
            } catch (CommandException e) {
                response = errorHandler.handle(request, e);
            }
        } else {
            response = errorHandler.handle(request, new CommandNotFoundException(request.getCommand()));
        }
        try {
            RemoteEndpoint remote = this.session.getRemote();
            remote.sendBytes(ByteBuffer.wrap(response.getResponse().toString().getBytes()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onWebSocketClose(int statusCode, String reason) {
        super.onWebSocketClose(statusCode, reason);
        log.debug("Socket {} closed", this.session.getRemoteAddress());
    }

    @Override
    public void onWebSocketError(Throwable cause) {
        super.onWebSocketError(cause);
        log.warn("Socket {} threw {}", this.session.getRemoteAddress(), cause.getClass().getSimpleName());
        log.debug("[TRACE]", cause);
    }

    public static class Servlet extends WebSocketServlet {
        @Override
        public void configure(WebSocketServletFactory factory) {
            factory.register(WSHandler.class);
        }
    }
}
