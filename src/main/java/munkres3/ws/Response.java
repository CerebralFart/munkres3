package munkres3.ws;

import lombok.Getter;
import lombok.Setter;

public class Response {
    @Getter
    protected String command;
    @Getter
    protected String status;
    @Setter
    @Getter
    protected Object response = "";

    protected Response(String command) {
        this.command = command;
        this.status = "okay";
    }
}
