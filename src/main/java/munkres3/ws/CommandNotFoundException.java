package munkres3.ws;

import lombok.Getter;

public class CommandNotFoundException extends CommandException {
    private static final long serialVersionUID = 7345072143911589897L;
    @Getter
    private final String command;

    public CommandNotFoundException(String command) {
        super("Could not find command " + command);
        this.command = command;
    }
}
