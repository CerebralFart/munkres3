package munkres3.ws;

import lombok.Getter;

import java.util.Arrays;
import java.util.List;

public class Request {
    static Request fromString(String request) {
        String[] words = request.split(" ");
        String command = words[0];
        List<String> params = Arrays.asList(words).subList(1, words.length);
        return new Request(command, params);
    }

    @Getter
    private final String command;
    @Getter
    private final List<String> params;

    private Request(String command, List<String> params) {
        this.command = command;
        this.params = params;
    }

    public Response createResponse() {
        return new Response(this.command);
    }
}
