package munkres3.ws;

public class CommandException extends Exception {
    private static final long serialVersionUID = -1989258288474912222L;

    public CommandException(String message) {
        super(message);
    }
}
