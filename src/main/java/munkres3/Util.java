package munkres3;

import lombok.extern.slf4j.Slf4j;

import java.net.URI;
import java.util.Collection;
import java.util.function.Function;

@Slf4j
public class Util {
    public static <T> T firstNonNull(T... values) {
        log.trace("Getting firstNonNull from {} values", values.length);
        for (T value : values) {
            if (value != null) {
                return value;
            }
        }
        return null;
    }

    public static boolean endsWithAnyOf(String string, Collection<String> list) {
        log.trace("Checking end of {} for {} elements", string, list.size());
        for (String end : list) {
            if (string.endsWith(end)) return true;
        }
        return false;
    }

    public static <T> String join(T[] elements, String glue, Function<? super T, String> mapper) {
        String[] mapped = new String[elements.length];
        for (int i = 0; i < elements.length; i++) {
            mapped[i] = mapper.apply(elements[i]);
        }
        return join(mapped, glue);
    }

    public static String join(String[] elements, String glue) {
        StringBuilder sb = new StringBuilder();
        boolean first = true;
        for (String el : elements) {
            if (first) {
                first = false;
            } else {
                sb.append(glue);
            }
            sb.append(el);
        }
        return sb.toString();
    }
}
