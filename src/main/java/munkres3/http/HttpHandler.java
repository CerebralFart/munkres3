package munkres3.http;

import lombok.extern.slf4j.Slf4j;
import org.eclipse.jetty.http.HttpFields;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Slf4j
public class HttpHandler extends AbstractHandler {
    private final StaticFileHandler staticHandler = new StaticFileHandler();
    private final DynamicFileHandler dynamicHandler = new DynamicFileHandler();

    public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException {
        //Abort if it is a WS request
        HttpFields fields = baseRequest.getHttpFields();
        if ("websocket".equals(fields.get("Upgrade"))) {
            log.debug("Upgrading connection to websocket ({})", baseRequest.getHttpChannel().getRemoteAddress());
            return;
        }

        String fileRequest = baseRequest.getRequestURI();

        PrintWriter writer = response.getWriter();
        if (this.staticHandler.canHandle(fileRequest)) {
            response.setStatus(200);
            writer.println(this.staticHandler.get(fileRequest));
        } else if (this.dynamicHandler.canHandle(fileRequest)) {
            response.setStatus(200);
            writer.println(this.dynamicHandler.get(fileRequest, request));
        } else {
            response.setStatus(404);
            writer.println(this.staticHandler.get("/404.html"));
        }

        baseRequest.setHandled(true);
    }
}
