package munkres3.http;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import munkres3.Util;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
class StaticFileHandler {
    private static final List<String> ignoredExtensions = Arrays.asList(".jsx", ".gitkeep");

    private static String normalizeFile(String name) {
        log.trace("Normalizing file {}", name);
        if (name.endsWith("/")) name += "index.html";
        return name;
    }

    private final Map<String, String> files;

    @SneakyThrows(IOException.class)
        //TODO this only works with a flat folder structure
    StaticFileHandler() {
        File folder = new File(this.getClass().getResource("/site/").getFile());
        File[] fileList = folder.listFiles();
        if (fileList == null || fileList.length == 0) {
            throw new ExceptionInInitializerError("No files found in resources/site");
        }
        this.files = new HashMap<>(fileList.length);
        log.debug("Loading {} files for StaticFileHandler", fileList.length);
        //TODO maybe move the allowable check before initializing the files HashMap
        for (File file : fileList) {
            String name = "/" + file.getName();
            if (Util.endsWithAnyOf(name, ignoredExtensions)) {
                String content = FileUtils.readFileToString(file, "UTF-8");
                log.trace("Loading {} ({} bytes)", name, content.length());
                this.files.put(name, content);
            }
        }
    }

    String get(String file) {
        log.debug("Retrieving static file {}", file);
        file = StaticFileHandler.normalizeFile(file);
        return this.files.get(file);
    }

    boolean canHandle(String file) {
        log.trace("Checking for static file {}", file);
        file = StaticFileHandler.normalizeFile(file);
        return this.files.containsKey(file);
    }
}
