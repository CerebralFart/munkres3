package munkres3.http;

import lombok.extern.slf4j.Slf4j;
import munkres3.http.dynamics.File;
import org.reflections.Reflections;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Slf4j
class DynamicFileHandler {
    private final Map<String, File> files;

    DynamicFileHandler() {
        Reflections ref = new Reflections("munkres3.http.dynamics");
        Collection<Class<? extends File>> classes = ref.getSubTypesOf(File.class);
        this.files = new HashMap<>(classes.size());
        for (Class<? extends File> cls : classes) {
            try {
                File instance = cls.getConstructor().newInstance();
                String name = instance.getName();
                if (this.files.containsKey(name)) {
                    log.error("Duplicate declaration for {} found ({}, {})",
                        name, this.files.get(name).getClass().getName(), cls.getName());
                    throw new ExceptionInInitializerError("Duplicate dynamic file declaration found");
                } else {
                    this.files.put(name, instance);
                }
            } catch (IllegalAccessException e) {
                log.warn("Dynamic file {} has no accessible no-arg constructor", cls.getName(), e);
            } catch (InstantiationException e) {
                log.warn("Could not instantiate dynamic file {}", cls.getName(), e);
            } catch (NoSuchMethodException e) {
                log.warn("Dynamic file {} has no no-arg constructor", cls.getName(), e);
            } catch (InvocationTargetException e) {
                log.warn("Creating dynamic file {} threw an exception", cls.getName(), e.getTargetException());
            }
        }
    }

    boolean canHandle(String file) {
        log.trace("Checking for dynamic file {}", file);
        return this.files.containsKey(file);
    }

    String get(String file, HttpServletRequest request) {
        log.debug("Retrieving dynamic file {}", file);
        return this.files.get(file).handle(request);
    }
}
