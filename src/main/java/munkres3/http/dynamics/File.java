package munkres3.http.dynamics;

import javax.servlet.http.HttpServletRequest;

public interface File {
    String getName();

    String handle(HttpServletRequest request);
}
