package munkres3.http.dynamics;

import javax.servlet.http.HttpServletRequest;

public class ConfigFile implements File {
    @Override
    public String getName() {
        return "/config.js";
    }

    @Override
    public String handle(HttpServletRequest request) {
        return "var config={};";
    }
}
