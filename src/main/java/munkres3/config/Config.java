package munkres3.config;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class Config {
    public static final URI defaultConfig;
    private static final Gson gson = new Gson();
    private static Map[] configMaps;

    static {
        try {
            defaultConfig = Config.class.getResource("/defaultConfig.json").toURI();
            setConfigFiles(new URI[]{
                new File("./config.json").toURI(),
                defaultConfig
            });
        } catch (URISyntaxException e) {
            throw new ExceptionInInitializerError(e);
        }
    }

    public static void setConfigFiles(URI[] files) {
        if (files == null || files.length == 0)
            throw new ExceptionInInitializerError("Tried initializing without files");
        ConfigError.setFiles(files);
        initializeMaps(files);
    }

    public static <T> T get(String key, Class<T> as) {
        log.trace("Getting config {} as {}", key, as.getName());
        Object val = get(key);
        if (as.equals(Integer.class) && val instanceof Number) {
            log.debug("Casting config {} to integer", key);
            return as.cast(((Number) val).intValue());
        } else {
            return as.cast(val);
        }
    }

    public static Object get(String key) {
        ConfigError lastError = null;
        log.trace("Getting config {}", key);
        for (Map configMap : configMaps) {
            try {
                return readFrom(key, configMap);
            } catch (ConfigError ce) {
                lastError = ce;
            }
        }
        throw lastError;
    }

    private static Object readFrom(String key, Map source) {
        try {
            String[] sections = key.split("\\.", 2);
            if (sections.length == 0 || sections[0].isEmpty()) {
                throw new MalformedConfigKeyError();
            }
            if (source.containsKey(sections[0])) {
                Object val = source.get(sections[0]);
                if (sections.length == 1) {
                    return val;
                } else if (val instanceof Map) {
                    return readFrom(sections[1], (Map) val);
                } else {
                    throw new ConfigNotFoundError();
                }
            } else {
                throw new ConfigNotFoundError();
            }
        } catch (ConfigError ce) {
            ce.setFullkey(key);
            throw ce;
        }
    }

    private static void initializeMaps(URI[] files) {
        configMaps = new Map[files.length];
        for (int i = 0; i < files.length; i++) {
            configMaps[i] = readConfig(files[i]);
        }
    }

    private static Map readConfig(URI uri) {
        try {
            String json = IOUtils.toString(uri, Charset.defaultCharset());
            return gson.fromJson(json, HashMap.class);
        } catch (IOException e) {
            log.info("Could not read {}", uri);
            return Collections.emptyMap();
        }
    }
}
