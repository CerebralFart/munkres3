package munkres3.config;

public class ConfigNotFoundError extends ConfigError {
    ConfigNotFoundError() {
        super("Key could not be found");
    }
}
