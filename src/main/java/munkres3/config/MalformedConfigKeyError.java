package munkres3.config;

public class MalformedConfigKeyError extends ConfigError {
    MalformedConfigKeyError() {
        super("Key was malformed");
    }
}
