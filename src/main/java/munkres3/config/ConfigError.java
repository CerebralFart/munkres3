package munkres3.config;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import munkres3.Util;

import java.io.File;
import java.net.URI;

@Slf4j
public class ConfigError extends Error {
    //TODO that shouldn't be static really
    @Setter
    private static URI[] files;
    private String fullkey = "";

    ConfigError(String msg) {
        super(msg);
    }

    void setFullkey(String fullkey) {
        this.fullkey = fullkey;
    }

    @Override
    public String getMessage() {
        String msg = this.fullkey.isEmpty() ? super.getMessage() : String.format("%s (key=%s)", super.getMessage(), this.fullkey);
        if (files != null) {
            msg += "\nSearched in " + Util.join(files, ", ", file -> String.format("%s (%s)",
                file.getPath(),
                new File(file).exists() ? "exists" : "absent"
            ));
        }
        return msg;
    }
}
