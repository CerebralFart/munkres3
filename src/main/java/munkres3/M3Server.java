package munkres3;

import munkres3.config.Config;
import munkres3.http.HttpHandler;
import munkres3.ws.WSHandler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

public class M3Server {
    public static void main(String[] args) {
        Server server = new Server();
        ServerConnector connector = new ServerConnector(server);
        connector.setPort(Config.get("port", Integer.class));
        server.addConnector(connector);

        HandlerCollection hc = new HandlerCollection();

        hc.addHandler(new HttpHandler());

        ServletContextHandler wsHandler = new ServletContextHandler(ServletContextHandler.SESSIONS);
        wsHandler.setContextPath("/");
        ServletHolder holderEvents = new ServletHolder("ws-events", WSHandler.Servlet.class);
        wsHandler.addServlet(holderEvents, "/*");
        hc.addHandler(wsHandler);

        server.setHandler(hc);

        try {
            server.start();
            server.join();
        } catch (Throwable t) {
            t.printStackTrace(System.err);
        }
    }
}
