package munkres3.context;

public enum State {
    INITIALIZING,
    VOTING,
    COMPUTING,
    DONE,
    ABORTED;

    public boolean canTransitionTo(State to) {
        //Always allow to abort, except from DONE
        if (to == ABORTED) return this != DONE && this != ABORTED;
        switch (this) {
            case INITIALIZING:
                return to == VOTING;
            case VOTING:
                return to == COMPUTING;
            case COMPUTING:
                return to == DONE;
            //Don't allow transitions after DONE and ABORTED
            case DONE:
            case ABORTED:
                return false;
        }
        return false;
    }
}
