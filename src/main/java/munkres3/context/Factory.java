package munkres3.context;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import munkres3.config.Config;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

@Slf4j
public class Factory {
    @Getter
    private static final Factory instance = new Factory();

    private final Map<String, Context> instances = new HashMap<>(1);
    private final CleanerThread cleaner = new CleanerThread(this);
    //TODO check if SecureRandom is neccesary here
    private final Random random = new Random();

    private Factory() {
        this.cleaner.start();
    }

    Collection<Context> getInstances() {
        return this.instances.values();
    }

    public Context createContext() {
        synchronized (this.instances) {
            String id = this.generateId();
            Context context = new Context(id);
            this.instances.put(id, context);
            return context;
        }
    }

    //TODO it is not ensured that the context is registered to this factory, ContextFactory is a singleton so it shouldn't make a difference
    void removeContext(Context context) {
        String id = context.getId();
        log.debug("Deregistering context {}", id);
        this.instances.remove(id);
    }

    String generateId() {
        int i = 0;
        int max = (int) StrictMath.pow(Config.get("id.radix", Integer.class), Config.get("id.size", Integer.class));

        while (i < Config.get("id.attempts", Integer.class)) {
            //TODO generation strategy is not guaranteed to be optimal as it is not aware of already assigned IDs.
            int id = this.random.nextInt(max);
            String suggestedId = Integer.toString(id, Config.get("id.radix", Integer.class));
            if (!this.instances.containsKey(suggestedId)) return suggestedId;
            i++;
        }
        //TODO maybe not crash the entire system
        throw new IdGenerationError();
    }

    public void shutdown() {
        this.cleaner.stop();
    }
}
