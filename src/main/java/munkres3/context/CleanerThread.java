package munkres3.context;


import lombok.extern.slf4j.Slf4j;
import munkres3.config.Config;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

@Slf4j
class CleanerThread {
    private static final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    private final Factory factory;
    private ScheduledFuture<?> future = null;

    CleanerThread(Factory factory) {
        this.factory = factory;
    }

    void start() {
        if (this.future == null) {
            log.info("Starting context cleaner");
            this.future = scheduler.scheduleAtFixedRate(this::clean, Config.get("context.cleaner.interval", Integer.class), Config.get("context.cleaner.interval", Integer.class), TimeUnit.MINUTES);
        }
    }

    void stop() {
        if (this.future != null) {
            log.info("Stopping context cleaner");
            this.future.cancel(false);
        }
    }

    private void clean() {
        log.info("Cleaning contexts");
        this.factory.getInstances().forEach(this::maybeClean);
    }

    private void maybeClean(Context context) {
        if (context.getCleanupTime().plus(Config.get("context.grace", Integer.class), ChronoUnit.MINUTES).isAfter(LocalDateTime.now())) {
            if (context.getState() == State.COMPUTING) {
                //Postpone removal if the context is still computing
                LocalDateTime cleanup = context.getCleanupTime().plus(Config.get("context.cleaner.extension", Integer.class), ChronoUnit.MINUTES);
                context.setCleanupTime(cleanup);
            } else {
                this.factory.removeContext(context);
            }
        }
    }
}
