package munkres3.context;

import munkres3.config.Config;

public class IdGenerationError extends Error {
    IdGenerationError() {
        super(String.format("Could not find ID in %d attempts.", Config.get("id.attempts", Integer.class)));
    }
}
