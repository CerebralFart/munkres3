package munkres3.context;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import munkres3.config.Config;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class Context {
    @Getter
    private final String id;
    private final List<Group> groups = new ArrayList<>();
    private final List<Project> projects = new ArrayList<>();
    @Getter
    @Setter
    private LocalDateTime cleanupTime = LocalDateTime.now().plus(Config.get("context.cleaner.timeout", Integer.class), ChronoUnit.HOURS);
    @Getter
    private State state = State.INITIALIZING;

    Context(String id) {
        this.id = id;
    }

    public Group createGroup() {
        log.debug("Creating group for context {}", this.id);
        Group group = new Group();
        this.groups.add(group);
        return group;
    }

    public Project createProject() {
        log.debug("Creating project for context {}", this.id);
        Project project = new Project();
        this.projects.add(project);
        return project;
    }

    public void setState(State newState) {
        if (this.state.canTransitionTo(newState)) {
            log.debug("Updating state of {} to {}", this.id, newState);
            this.state = newState;
        } else {
            log.warn("Attempted to update state of {} from {} to {}", this.id, this.state, newState);
            throw new IllegalStateException(String.format("Cannot transition from %s to %s", this.state, newState));
        }
    }
}
