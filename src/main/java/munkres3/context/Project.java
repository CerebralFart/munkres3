package munkres3.context;

import lombok.Getter;
import lombok.Setter;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.OptionalInt;
import java.util.Set;

public class Project {
    private OptionalInt places;
    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private String company;
    @Getter
    @Setter
    private String description;
    private Set<Group> assignedGroups = new HashSet<>();

    Project() {
    }

    public void setInfinitePlaces() {
        this.places = OptionalInt.empty();
    }

    public void setPlaces(int places) {
        this.places = OptionalInt.of(places);
    }

    public boolean hasInfinitePlaces() {
        return !this.places.isPresent();
    }

    public int getPlaces() {
        return this.places.orElse(-1);
    }

    public void assignGroup(Group group) {
        this.assignedGroups.add(group);
    }

    public Collection<Group> getAssignedGroups() {
        return Collections.unmodifiableCollection(this.assignedGroups);
    }
}
