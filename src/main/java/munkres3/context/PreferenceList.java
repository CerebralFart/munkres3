package munkres3.context;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.LinkedList;

@Slf4j
public class PreferenceList {
    @Getter
    private int maxSize;
    private final LinkedList<Project> list = new LinkedList<>();

    public PreferenceList(int maxSize) {
        this.maxSize = maxSize;
    }

    public void setMaxSize(int size) {
        if (size != this.maxSize) {
            log.debug("Setting max size of PreferenceList@{} to {}", this.hashCode(), size);
            this.maxSize = size;
            this.cleanList();
        }
    }

    public void insertAt(Project project, int index) {
        if (index < this.maxSize) {
            if (this.list.contains(project)) {
                this.remove(project);
            }

            if (index > this.list.size()) {
                this.list.add(project);
            } else {
                this.list.add(index, project);
            }
            this.cleanList();
        }
    }

    public void remove(Project project) {
        this.list.remove(project);
    }

    public int size() {
        return this.list.size();
    }

    public Project get(int index) {
        return this.list.get(index);
    }

    public int indexOf(Project project) {
        return this.list.indexOf(project);
    }

    private void cleanList() {
        int size = this.list.size();
        while (size > this.maxSize) {
            this.list.removeLast();
            size--;
        }
    }
}

