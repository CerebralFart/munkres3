package munkres3.config;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

public class ConfigTest {
    @BeforeAll
    static void initialize() throws URISyntaxException {
        URI configFile = ConfigTest.class.getResource("/testConfig.json").toURI();
        Config.setConfigFiles(new URI[]{configFile});
    }

    @AfterAll
    static void clean() {
        Config.setConfigFiles(new URI[]{
            Config.defaultConfig
        });
    }

    @Test
    void testNotNested() {
        Assertions.assertEquals(8192, Config.get("int", Integer.class));
        Assertions.assertEquals("value", Config.get("string", String.class));
        try {
            Config.get("missing");
            Assertions.fail();
        } catch (ConfigError ce) {
            String msg = ce.getMessage();
            Assertions.assertTrue(msg.contains("missing"));
        }
    }

    @Test
    void testNested() {
        Assertions.assertEquals(4096, Config.get("nest.int", Integer.class));
        Assertions.assertEquals("nested value", Config.get("nest.string", String.class));
        try {
            Config.get("nest.missing");
            Assertions.fail();
        } catch (ConfigError ce) {
            String msg = ce.getMessage();
            Assertions.assertTrue(msg.contains("nest.missing"));
        }
    }

    @Test
    void testGetMap() {
        Map map = Config.get("nest", Map.class);
        Assertions.assertEquals(2, map.size());
    }

    @Test
    void testNestedNonMap() {
        Assertions.assertThrows(ConfigNotFoundError.class, () -> {
            Config.get("int.nested");
        });
    }

    @Test
    void testMalformedKey() {
        Assertions.assertThrows(MalformedConfigKeyError.class, () -> {
            Config.get("nest.");
        });
    }

    @Test
    void testLoadConfigFiles() {
        Assertions.assertThrows(ExceptionInInitializerError.class, () -> {
            Config.setConfigFiles(null);
        });
        Assertions.assertThrows(ExceptionInInitializerError.class, () -> {
            Config.setConfigFiles(new URI[]{});
        });
    }
}
