package munkres3.logging;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.OutputStream;
import java.io.PrintStream;

@Slf4j
class DebugFilterTest {
    private static PrintStream sysOut;
    private static boolean gotCalled;

    @BeforeAll
    static void initialize() {
        sysOut = System.out;
        System.setOut(new PrintStream(new OutputStream() {
            @Override
            public void write(int b) {
                gotCalled = true;
            }
        }, true));
    }

    @AfterAll
    static void deinit() {
        System.setOut(sysOut);
    }

    @BeforeEach
    void setup() {
        gotCalled = false;
    }

    @Test
    void testTrace() {
        log.trace("Trace test");
        Assertions.assertFalse(gotCalled);
    }

    @Test
    void testDebug() {
        log.debug("Debug test");
        Assertions.assertFalse(gotCalled);
    }

    @Test
    void testInfo() {
        log.info("Info test");
        Assertions.assertTrue(gotCalled);
    }

    @Test
    void testWarn() {
        log.warn("Warn test");
        Assertions.assertTrue(gotCalled);
    }

    @Test
    void testError() {
        log.error("Error test");
        Assertions.assertTrue(gotCalled);
    }
}
