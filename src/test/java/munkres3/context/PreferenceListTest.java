package munkres3.context;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PreferenceListTest {
    private PreferenceList list;

    @BeforeEach
    void setup() {
        list = new PreferenceList(5);
    }

    @Test
    void testGetMaxSize() {
        Assertions.assertEquals(5, list.getMaxSize(), "Expected default max size");
    }

    @Test
    void testSetMaxSize() {
        list.setMaxSize(7);
        Assertions.assertEquals(7, list.getMaxSize(), "Expected max size to be updated");
    }

    @Test
    void testSetMaxSizeFilledList() {
        Project project1 = new Project();
        list.insertAt(project1, 0);
        Project project2 = new Project();
        list.insertAt(project2, 1);
        Project project3 = new Project();
        list.insertAt(project3, 2);
        Project project4 = new Project();
        list.insertAt(project4, 3);
        Project project5 = new Project();
        list.insertAt(project5, 4);
        list.setMaxSize(3);
        Assertions.assertEquals(project1, list.get(0));
        Assertions.assertEquals(project2, list.get(1));
        Assertions.assertEquals(project3, list.get(2));
        Assertions.assertEquals(3, list.size());
    }

    @Test
    void testAddProject() {
        Assertions.assertEquals(0, list.size(), "Expected empty list");
        Project project1 = new Project();
        list.insertAt(project1, 0);
        Assertions.assertEquals(1, list.size(), "Expected list to contain 1 item");
    }

    @Test
    void testAddProjectWReordering() {
        Project project1 = new Project();
        Project project2 = new Project();
        list.insertAt(project1, 0);
        list.insertAt(project2, 0);
        Assertions.assertEquals(project1, list.get(1));
        Assertions.assertEquals(project2, list.get(0));
    }

    @Test
    void testGet() {
        Project project1 = new Project();
        Project project2 = new Project();
        list.insertAt(project1, 0);
        list.insertAt(project2, 1);
        Assertions.assertEquals(project1, list.get(0));
        Assertions.assertEquals(project2, list.get(1));
    }

    @Test
    void testGetBumpedProject() {
        Project project1 = new Project();
        list.insertAt(project1, 0);
        Project project2 = new Project();
        list.insertAt(project2, 0);
        Project project3 = new Project();
        list.insertAt(project3, 0);
        Project project4 = new Project();
        list.insertAt(project4, 0);
        Project project5 = new Project();
        list.insertAt(project5, 0);
        Project project6 = new Project();
        list.insertAt(project6, 0);
        Assertions.assertEquals(5, list.size());
        Assertions.assertEquals(-1, list.indexOf(project1));
        Assertions.assertEquals(4, list.indexOf(project2));
        Assertions.assertEquals(3, list.indexOf(project3));
        Assertions.assertEquals(2, list.indexOf(project4));
        Assertions.assertEquals(1, list.indexOf(project5));
        Assertions.assertEquals(0, list.indexOf(project6));
    }

    @Test
    void testSize() {
        Assertions.assertEquals(0, list.size(), "Expected empty list");
        Project project1 = new Project();
        list.insertAt(project1, 0);
        Assertions.assertEquals(1, list.size(), "Expected list to contain 1 item");
        Project project2 = new Project();
        list.remove(project2);
        Assertions.assertEquals(1, list.size(), "Expected list to still contain 1 item");
        list.remove(project1);
        Assertions.assertEquals(0, list.size(), "Expected list to be emptied");
    }

    @Test
    void testSizeAfterResize() {
        list.insertAt(new Project(), 0);
        list.insertAt(new Project(), 0);
        list.insertAt(new Project(), 0);
        list.insertAt(new Project(), 0);
        list.insertAt(new Project(), 0);
        Assertions.assertEquals(5, list.size());
        list.setMaxSize(3);
        Assertions.assertEquals(3, list.size());
    }

    @Test
    void testRemove() {
        Project project1 = new Project();
        Project project2 = new Project();
        Project project3 = new Project();
        list.insertAt(project1, 0);
        list.insertAt(project2, 0);
        list.insertAt(project3, 0);
        Assertions.assertEquals(3, list.size());
        list.remove(project2);
        Assertions.assertEquals(project1, list.get(1));
        Assertions.assertEquals(project3, list.get(0));
    }

    @Test
    void testInsert() {
        Assertions.assertEquals(0, list.size(), "Expected empty list");
        Project project1 = new Project();
        list.insertAt(project1, 0);
        Assertions.assertEquals(1, list.size(), "Expected list to contain 1 item");
        Assertions.assertEquals(0, list.indexOf(project1), "Expected project to be at the top of the list");
    }

    @Test
    void testInsertNotConnected() {
        Assertions.assertEquals(0, list.size(), "Expected empty list");
        Project project1 = new Project();
        list.insertAt(project1, 3);
        Assertions.assertEquals(1, list.size(), "Expected list to contain 1 item");
        Assertions.assertEquals(0, list.indexOf(project1), "Expected project to be at the top of the list");
    }

    @Test
    void testInsertPastMaxSize() {
        Assertions.assertEquals(0, list.size(), "Expected empty list");
        Project project1 = new Project();
        list.insertAt(project1, 6);
        Assertions.assertEquals(0, list.size(), "Expected list remain empty when adding outside bounds");
    }

    @Test
    void testDuplicateInsertion() {
        Project project1 = new Project();
        Project project2 = new Project();
        list.insertAt(project1, 0);
        list.insertAt(project2, 0);
        list.insertAt(project1, 0);
        Assertions.assertEquals(2, list.size());
        Assertions.assertEquals(0, list.indexOf(project1));
        Assertions.assertEquals(1, list.indexOf(project2));
    }
}

