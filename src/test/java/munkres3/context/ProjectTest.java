package munkres3.context;

import munkres3.config.Config;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.net.URI;
import java.net.URISyntaxException;

public class ProjectTest {
    private Project project;

    @BeforeAll
    static void init() throws URISyntaxException {
        Config.setConfigFiles(new URI[]{Config.defaultConfig});
    }

    @BeforeEach
    void setup() {
        this.project = new Project();
    }

    @Test
    void testPlaces() {
        this.project.setInfinitePlaces();
        Assertions.assertTrue(this.project.hasInfinitePlaces());
        Assertions.assertEquals(-1, this.project.getPlaces());
        this.project.setPlaces(5);
        Assertions.assertFalse(this.project.hasInfinitePlaces());
        Assertions.assertEquals(5, this.project.getPlaces());
    }

    @Test
    void testAssignment() {
        Group g1 = new Group();
        Group g2 = new Group();
        Assertions.assertEquals(0, this.project.getAssignedGroups().size());

        this.project.assignGroup(g1);
        Assertions.assertEquals(1, this.project.getAssignedGroups().size());
        Assertions.assertTrue(this.project.getAssignedGroups().contains(g1));

        this.project.assignGroup(g2);
        Assertions.assertEquals(2, this.project.getAssignedGroups().size());
        Assertions.assertTrue(this.project.getAssignedGroups().contains(g1));
        Assertions.assertTrue(this.project.getAssignedGroups().contains(g2));

        this.project.assignGroup(g1);
        Assertions.assertEquals(2, this.project.getAssignedGroups().size());
        Assertions.assertTrue(this.project.getAssignedGroups().contains(g1));
        Assertions.assertTrue(this.project.getAssignedGroups().contains(g2));
    }

    @Test
    void testName() {
        String val = "TestName";
        this.project.setName(val);
        Assertions.assertEquals(val, this.project.getName(), "Expected getter-setter pair to work for name");
    }

    @Test
    void testCompany() {
        String val = "TestCompany";
        this.project.setCompany(val);
        Assertions.assertEquals(val, this.project.getCompany(), "Expected getter-setter pair to work for company");
    }

    @Test
    void testDescription() {
        String val = "TestDescription";
        this.project.setDescription(val);
        Assertions.assertEquals(val, this.project.getDescription(), "Expected getter-setter pair to work for description");
    }
}
