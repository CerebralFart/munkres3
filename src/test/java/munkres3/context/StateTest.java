package munkres3.context;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.*;

public class StateTest {
    @Test
    void testCanTransitionTo() {
        Map<State, List<State>> allowedTransitions = new HashMap<>(State.values().length);
        allowedTransitions.put(State.INITIALIZING, Arrays.asList(State.VOTING, State.ABORTED));
        allowedTransitions.put(State.VOTING, Arrays.asList(State.COMPUTING, State.ABORTED));
        allowedTransitions.put(State.COMPUTING, Arrays.asList(State.DONE, State.ABORTED));
        allowedTransitions.put(State.DONE, Collections.emptyList());
        allowedTransitions.put(State.ABORTED, Collections.emptyList());

        for (State from : State.values()) {
            for (State to : State.values()) {
                boolean expected = allowedTransitions.get(from).contains(to);
                boolean actual = from.canTransitionTo(to);
                Assertions.assertEquals(
                    expected,
                    actual,
                    String.format("Transition from %s to %s, expected %b, was %b", from, to, expected, actual)
                );
            }
        }
    }
}
