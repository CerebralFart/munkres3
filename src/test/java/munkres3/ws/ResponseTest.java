package munkres3.ws;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ResponseTest {
    private final Request request = Request.fromString("test");
    private final Response response = this.request.createResponse();

    @Test
    void testCommandCopy() {
        Assertions.assertEquals("test", this.response.getCommand());
    }

    @Test
    void testStatus() {
        Assertions.assertEquals("okay", this.response.getStatus());
    }

    @Test
    void testResponse() {
        this.response.setResponse(5);
        Assertions.assertEquals(5, this.response.getResponse());
    }
}
