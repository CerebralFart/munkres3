package munkres3.ws.handlers;

import munkres3.ws.CommandException;
import munkres3.ws.Common;
import munkres3.ws.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class PingHandlerTest {
    private Handler handler = new PingHandler();

    @Test
    void testGetKeyword() {
        Assertions.assertEquals("ping", this.handler.getKeyword());
    }

    @Test
    void testHandle() throws CommandException {
        Response response = this.handler.handle(Common.EMPTY_REQUEST);
        Assertions.assertEquals("pong", response.getResponse());
        Assertions.assertEquals("okay", response.getStatus());
    }
}
