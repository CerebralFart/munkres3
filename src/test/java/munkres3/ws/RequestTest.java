package munkres3.ws;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

public class RequestTest {
    @Test
    void testRequestNoParams() {
        Request request = Request.fromString("test");
        Assertions.assertEquals("test", request.getCommand());
        Assertions.assertLinesMatch(Collections.emptyList(), request.getParams());
    }

    @Test
    void testRequestWParams() {
        Request request = Request.fromString("test 1 2 3");
        Assertions.assertEquals("test", request.getCommand());
        Assertions.assertLinesMatch(Arrays.asList("1", "2", "3"), request.getParams());
    }

    @Test
    void testCreateResponse() {
        Request request = Request.fromString("test");
        Response response = request.createResponse();
        Assertions.assertEquals("test", response.getCommand());
    }
}
