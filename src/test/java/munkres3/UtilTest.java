package munkres3;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

class UtilTest {
    @Test
    void firstNonNullTest() {
        Assertions.assertEquals(
            1,
            Util.firstNonNull(1, 2, 3)
        );
        Assertions.assertEquals(
            2,
            Util.firstNonNull(null, 2, null)
        );
        Assertions.assertEquals(
            3,
            Util.firstNonNull(null, null, 3)
        );
        Assertions.assertNull(Util.firstNonNull());
        Assertions.assertNull(Util.firstNonNull(null, null, null));
    }

    @Test
    void testEndsWithAnyOf(){
        Assertions.assertTrue(Util.endsWithAnyOf("ping", Arrays.asList("ing", "ong")));
        Assertions.assertTrue(Util.endsWithAnyOf("pong", Arrays.asList("ing", "ong")));
        Assertions.assertFalse(Util.endsWithAnyOf("pang", Arrays.asList("ing", "ong")));
    }
}
